class Cell {
	static Dead = new Cell('DEAD');
	static Alive = new Cell('ALIVE');

	private constructor(private state: string) {}

	public nextState(nbNeighbour: number) {
		if (this.state === 'ALIVE') {
			if (nbNeighbour === 2 || nbNeighbour === 3) {
				return Cell.Alive;
			}
		} else if (nbNeighbour === 3) {
			return Cell.Alive;
		}
		return Cell.Dead;
	}
}

describe('Game of life', () => {
	describe('Cell', () => {
		it('should be dead', () => {
			const $cell = Cell.Dead;
			const $nextCell = $cell.nextState(0);
			expect($nextCell).toBe(Cell.Dead);
		});

		it('should remain dead when 2 neighbours', () => {
			const $cell = Cell.Dead;
			const $nextCell = $cell.nextState(2);
			expect($nextCell).toBe(Cell.Dead);
		});

		it('should die when too many neighbours', () => {
			const $cell = Cell.Alive;
			const $nextCell = $cell.nextState(4);
			expect($nextCell).toBe(Cell.Dead);
		});

		it('should live when 2 neighbours', () => {
			const $cell = Cell.Alive;
			const $nextCell = $cell.nextState(2);
			expect($nextCell).toBe(Cell.Alive);
		});

		it('should live when 3 neighbours', () => {
			const $cell = Cell.Alive;
			const $nextCell = $cell.nextState(3);
			expect($nextCell).toBe(Cell.Alive);
		});

		it('should be born when 3 neighbours', () => {
			const $cell = Cell.Dead;
			const $nextCell = $cell.nextState(3);
			expect($nextCell).toBe(Cell.Alive);
		});
	});

	describe('Name of the group', () => {
		it('should own no cell', () => {
			const grid = new Grid(5);
			const nbNeighbour = grid.countNeighbours(1, 1);
			expect(nbNeighbour).toBe(0);
		});
	});
});
