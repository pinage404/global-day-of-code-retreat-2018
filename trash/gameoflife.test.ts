class Cell {
	constructor(public state: string, public x: number, public y: number) {}

	act(grid: Cell[]) {
		if (grid.length < 2 + 1 || grid.length > 3 + 1) {
			this.state = 'dead';
		}
	}
}

class Grid {
	constructor(public livingCells: Cell[]) {}

	act() {
		if (grid.length < 2 + 1 || grid.length > 3 + 1) {
			this.state = 'dead';
		}
	}

	getCell(x: number, y: number) {
		return this.livingCells.find(c => c.x === x && c.y === y);
	}

	static GridFactory(grid?: Grid) {
		if (typeof grid === 'undefined') {
			return new Grid([]);
		}
		return new Grid(grid.livingCells);
	}
}
describe('should suicide when less than two neighbours', () => {
	it('when 0 neighbours', () => {
		const cell = new Cell('alive', 0, 0);
		const grid = [cell];

		cell.act(grid);

		expect(cell.state).toBe('dead');
	});

	it('when 1 neighbour', () => {
		const cell = new Cell('alive', 0, 0);
		const grid = [cell, new Cell('alive', 0, 1)];

		cell.act(grid);

		expect(cell.state).toBe('dead');
	});
});

describe('should keep alive when two or three neighbours', () => {
	it('when 2 neighbours', () => {
		const cell = new Cell('alive', 0, 0);
		const grid = [cell, new Cell('alive', 0, 1), new Cell('alive', 1, 1)];

		cell.act(grid);

		expect(cell.state).toBe('alive');
	});

	it('when 3 neighbours', () => {
		const cell = new Cell('alive', 0, 0);
		const grid = [
			cell,
			new Cell('alive', 0, 1),
			new Cell('alive', 1, 0),
			new Cell('alive', 1, 1)
		];

		cell.act(grid);

		expect(cell.state).toBe('alive');
	});
});

describe('should be smatch when too many neighbours', () => {
	it('when 4 neighbours', () => {
		const cell = new Cell('alive', 0, 0);
		const grid = [
			cell,
			new Cell('alive', 0, -1),
			new Cell('alive', 0, 1),
			new Cell('alive', 1, 0),
			new Cell('alive', 1, 1)
		];

		cell.act(grid);

		expect(cell.state).toBe('dead');
	});
});

describe('should go alive when exactly enough neighbours', () => {
	it('when 3 neighbours', () => {
		const grid = [
			new Cell('alive', 0, -1),
			new Cell('alive', 0, 1),
			new Cell('alive', 1, 0)
		];

		grid.act();

		expect(grid.getCell(0, 0)).toBe('alive');
	});
});
