abstract class _Cell {
	constructor(protected state: string) {}

	abstract match<R>(aliveCase: (Alive) => R, deadCase: (Dead) => R): R;

	nextState(neighbours: Cell[]): Cell {
		if (this.countNeighbours(neighbours) < 2) {
			return Cell.Dead;
		}
		if (this.countNeighbours(neighbours) > 3) {
			return Cell.Dead;
		}
		return this;
	}

	private countNeighbours(neighbours: Cell[]): number {
		return neighbours
			.filter(cell => cell === Cell.Alive)
			.reduce<number>((acc, _) => acc + 1, 0);
	}
}

class Alive extends _Cell {
	match<R>(aliveCase: (Alive: any) => R, deadCase: (Dead: any) => R): R {
		return aliveCase(this);
	}
}

class Dead extends _Cell {
	match<R>(aliveCase: (Alive: any) => R, deadCase: (Dead: any) => R): R {
		return deadCase(this);
	}
}

class Cell {
	static Alive = new Alive('alive');
	static Dead = new Dead('dead');
}

describe('Cell', () => {
	it('should remain dead when 2 neighbours', () => {
		const cell = Cell.Dead;
		const neighbours = [
			Cell.Alive,
			Cell.Alive,
			Cell.Dead,
			Cell.Dead,
			Cell.Dead,
			Cell.Dead,
			Cell.Dead,
			Cell.Dead
		];
		const newState = cell.nextState(neighbours);
		expect(newState).toEqual(Cell.Dead);
	});

	it('should remain alive when 2 neighbours', () => {
		const cell = Cell.Alive;
		const neighbours = [
			Cell.Alive,
			Cell.Alive,
			Cell.Dead,
			Cell.Dead,
			Cell.Dead,
			Cell.Dead,
			Cell.Dead,
			Cell.Dead
		];
		const newState = cell.nextState(neighbours);
		expect(newState).toEqual(Cell.Alive);
	});

	it('should die when less than 2 neighbours', () => {
		const cell = Cell.Alive;
		const neighbours = [
			Cell.Alive,
			Cell.Dead,
			Cell.Dead,
			Cell.Dead,
			Cell.Dead,
			Cell.Dead,
			Cell.Dead,
			Cell.Dead
		];
		const newState = cell.nextState(neighbours);
		expect(newState).toEqual(Cell.Dead);
	});

	it('should die when more than 3 neighbours', () => {
		const cell = Cell.Alive;
		const neighbours = [
			Cell.Alive,
			Cell.Alive,
			Cell.Alive,
			Cell.Alive,
			Cell.Dead,
			Cell.Dead,
			Cell.Dead,
			Cell.Dead
		];
		const newState = cell.nextState(neighbours);
		expect(newState).toEqual(Cell.Dead);
	});
});
